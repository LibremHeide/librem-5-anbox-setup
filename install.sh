#!/bin/bash
#Check you kernel supports anbox:
URL="https://dosowisko.net/anbox/arm64/android.img"

#Check for root
if [ "$EUID" -ne 0 ]
  then echo "Please run with sudo"
  exit 3
fi

#Check for the right kernel
if [ ! -e "/dev/binder" ]; then
    echo "Your kernel does not support Anbox. Please update your system."
    exit 1
fi

#This is how anbox is installed
function run_install 
{
    echo "Adding repo:"
    echo "deb [trusted=yes] http://deb.debian.org/debian buster main contrib" | tee -a /etc/apt/sources.list
    apt update
    apt install anbox android-tools-adb
    echo "Removing repo:"
    cat /etc/apt/sources.list | head -n -1 | tee /etc/apt/sources.list
}

function download_image
{
    echo "Downloading image..."
    wget $URL
    mv ./android.img /var/lib/anbox/android.img
}

#Update anbox from debian contrib
function update_anbox 
{
    echo "Adding repo:"
    echo "deb [trusted=yes] http://deb.debian.org/debian buster main contrib" | tee -a /etc/apt/sources.list
    apt update
    apt update anbox android-tools-adb
    echo "Removing repo:"
    cat /etc/apt/sources.list | head -n -1 | tee /etc/apt/sources.list
}

#Ask the user if they want to continue
while :
do
    #Swich to RED
    echo -e '\033[31m'
    echo -ne "Warning, this will temporarily add:\n'http://deb.debian.org/debian buster main contrib'\nThis software repo is not curated by Purism, installing may damage your system. \n\nAn Android image will also be downloaded from $URL.\nWould you like to continue? [Yes/No]: "
    read  REPLY
    echo -e '\033[0m'
    if [[ $REPLY = "Yes" ]]
    then
        #start install
        run_install
        download_image
        exit 0

    elif [[ $REPLY = "No" ]]
    then
        exit 2
    fi
done
