#!/bin/bash
if [ "$EUID" == "0" ]
then 
  echo "Don't run with sudo or as root"
  exit
fi

sudo anbox container-manager --data-path=/var/lib/anbox --daemon &
sleep 3
DISPLAY=:0 SDL_VIDEODRIVER=x11 anbox session-manager &


#setup Network
sudo update-alternatives --set iptables /usr/sbin/iptables-legacy
sudo nmcli con delete anbox-net
sudo nmcli con add type bridge ifname anbox0 -- connection.id anbox-net ipv4.method shared ipv4.addresses 192.168.250.1/24
if ! command -v anbox-bridge.sh &> /dev/null
then
    echo "Could not find anbox-bridge.sh. Downloading..."
    sudo wget https://raw.githubusercontent.com/Debyzulkarnain/anbox-bridge/master/anbox-bridge.sh -O /usr/sbin/anbox-bridge.sh
    sudo chmod 755 /usr/sbin/anbox-bridge.sh
fi
sudo /usr/sbin/anbox-bridge.sh start

while :
do
    outcome=$(sudo lxc-attach -q --clear-env -P /var/lib/anbox/containers -n default -v PATH=/sbin:/system/bin:/system/sbin:/system/xbin -v ANDROID_ASSETS=/assets -v ANDROID_DATA=/data -v ANDROID_ROOT=/system -v ANDROID_STORAGE=/storage -v ASEC_MOUNTPOINT=/mnt/asec -v EXTERNAL_STORAGE=/sdcard -- /system/bin/sh -c "ip route add default dev eth0 via 192.168.250.1" 2>&1)
    if [[ $outcome == "RTNETLINK answers: File exists" ]]
    then
        echo "Network setup!"
        echo -e "You can setup F-Droid with\nwget https://f-droid.org/F-Droid.apk\nadb install F-Droid.apk"
        break
    else
        echo -n "."
        sleep .2
fi
done
